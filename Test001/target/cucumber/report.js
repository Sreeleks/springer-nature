$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Feature/Search.feature");
formatter.feature({
  "line": 2,
  "name": "Search",
  "description": "",
  "id": "search",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 5,
  "name": "Search an input",
  "description": "",
  "id": "search;search-an-input",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "User is logged in homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "User input keyword and click Search icon",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "search suggestion result should be displayed",
  "keyword": "Then "
});
});