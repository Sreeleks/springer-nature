import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.google.gson.JsonObject;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class StepDefs {

	WebDriver driver ; 

@Before
public void setEnvironmentFile(){
 System.setProperty("webdriver.chrome.driver", "D:\\chromedriver_win32\\chromedriver.exe");
}
 
@Given("^User is logged in homepage$")
public void User_is_logged_in_homepage() throws Throwable {
	
    WebDriver driver = new ChromeDriver(); 
    driver.get("https://link.springer.com"); 
    System.out.println("Successful Access");
}

 
@When("^User input keyword and click Search icon$")
public void User_input_keyword() throws Throwable {
	WebElement element = driver.findElement(By.id("query"));
    element.sendKeys("Medicine");
    driver.findElement(By.id("search"));
}

@Then("^search suggestion result should be displayed$")
    public void suggestion_result_display() throws Throwable {
	WebElement searchResult = driver.findElement(By.xpath("//div[@input='Medicine']"));
	System.out.println("Results for you");
    }
    

}
